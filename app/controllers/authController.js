const db = require("../models");
const bcrypt = require("bcrypt");
const jwt=require('jsonwebtoken');


const signUp = async (req, res) => {
    try {
        const userRole = await db.role.findOne({
            name: "user"
        })

        const user = new db.user({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 8),
            roles: [
                userRole._id
            ]
        })
        await user.save();
        res.status(200).json({
            message:"create user successfully"
        })
    }
    catch (error) {
        res.status(500).json({
            message:"Internal server error"
        })
    }

}

const login=async (req,res)=>{
    try{
        const exitUser=await db.user.findOne({
            username:req.body.username
        }).populate("roles");

       if(!exitUser)
       {
        return res.status(404).send({
            message:"User not found"
        })
       }

       var passwordIsValid=bcrypt.compareSync(
            req.body.password,
            exitUser.password
       )

       if(!passwordIsValid)
       {
        return res.status(401).json({
            message:"Invalid password"
        })
       }
      
       const secretKey=process.env.JWT_SECRET;
       const token=jwt.sign({id:exitUser._id},secretKey,{
        algorithm:"HS256",
        allowInsecureKeySizes:true,
        expiresIn:86400 //1 ngay
       }
    );

    res.status(200).json({
        accessToken:token
    })
    }
    catch(error){
        res.status(500).json({
            message:"Intenal sever error"
        })
    }
}

module.exports = {
    signUp,
    login
}