const db=require("../models");

const user=db.user;
const ROLES=db.ROLES;

const checkDuplicateUsername= async(req,res,next)=>{
    try{
    const existUser= await user.findOne({
        username:req.body.username
    });
    if(existUser){
        res.status(400).send({
            message:"Username is already in use"
        })
        return;
    }
    next();
}
catch(err)
{
    console.error("Interal server error",err);
    process.exit();
}
}

module.exports={checkDuplicateUsername}