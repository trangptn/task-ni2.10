const mongoose=require('mongoose');

const userSchema= new mongoose.Schema({
    username:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    roles:[
    {
        type:String,
        ref:"role"
    }
]
},
{
    timestamps:true
})

module.exports=mongoose.model("user",userSchema);