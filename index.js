const express=require('express');

const cors= require('cors');

require("dotenv").config();

const db=require("./app/models");

const {initial}=require("./data");

const app=express();

app.use(cors());

app.use(express.json());

db.mongoose.connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`)
.then(()=>{
    console.log("Connect mongoDb Successfully");
    initial();
})
.catch((error)=>{
    console.error("Connect error",error);
    process.exit();
})

app.get("/",(req,res)=>{
    res.json({
        message:"Wellcome to Devcamp JWT"
    })
})

app.use("/api/auth/",require("./app/routers/authRouter"))

const PORT =process.env.ENV_PORT || 7000;

app.listen(PORT,()=>{
    console.log(`App listening on port ${PORT}` );
})


